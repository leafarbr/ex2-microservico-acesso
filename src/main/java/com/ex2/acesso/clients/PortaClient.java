package com.ex2.acesso.clients;


import com.ex2.acesso.dtos.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "porta", configuration = UsuarioClientConfiguration.class)
public interface PortaClient {

    @GetMapping("porta/{id}")
    Porta consultarPortaPorId(@PathVariable int id);

}
