package com.ex2.acesso.clients;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class PortaClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new UsuarioClientDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new UsuarioClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }

}
