package com.ex2.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta não encontrada")
public class PortaClientExcepetion extends RuntimeException{
}
