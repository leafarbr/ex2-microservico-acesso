package com.ex2.acesso.clients;

import com.ex2.acesso.dtos.Porta;

public class PortaClientFallBack implements PortaClient {

    @Override
    public Porta consultarPortaPorId(int id) {

        Porta objPorta = new Porta();

        objPorta.setId(1);
        objPorta.setAndar("1");
        objPorta.setSala("1000A");

        return objPorta;

    }
}
