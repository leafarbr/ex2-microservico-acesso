package com.ex2.acesso.clients;

import com.ex2.acesso.dtos.Usuario;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "usuario", configuration = UsuarioClientConfiguration.class)
public interface UsuarioClient {

        @GetMapping("usuario/{id}")
        Usuario consultarUsuarioPorId(@PathVariable int id);

}

