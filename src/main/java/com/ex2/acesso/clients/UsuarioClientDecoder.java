package com.ex2.acesso.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class UsuarioClientDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new UsuarioClientException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
