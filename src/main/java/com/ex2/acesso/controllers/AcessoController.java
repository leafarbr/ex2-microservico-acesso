package com.ex2.acesso.controllers;

import com.ex2.acesso.clients.PortaClient;
import com.ex2.acesso.clients.UsuarioClient;
import com.ex2.acesso.dtos.Porta;
import com.ex2.acesso.dtos.Usuario;
import com.ex2.acesso.models.Acesso;
import com.ex2.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Acesso")
public class AcessoController
{

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private UsuarioClient usuarioClient;

    @Autowired
    private PortaClient portaClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Acesso cadastrarAcesso(@RequestBody Acesso pacesso){

        //Verificar Usuario
        Usuario objUsuario = usuarioClient.consultarUsuarioPorId(pacesso.getUsuario_id());

        //Verificar Porta
        Porta objPorta = portaClient.consultarPortaPorId(pacesso.getPorta_id());

        return acessoService.cadastrarAcesso(pacesso);

    }


    @GetMapping("/{cliente_id}/{porta_id}")
    Acesso consultarPorId(@PathVariable(name = "client_id") int idClient, @PathVariable(name = "porta_id") int idPorta) {

            return acessoService.consultarAcessoPorPortaeUsuario(idPorta, idClient);

    }


}
