package com.ex2.acesso.repositories;

import com.ex2.acesso.models.Acesso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    public Iterable<Acesso> findByUsuario_id(int idUsuario);
    public Iterable<Acesso> findByPorta_id (int idPorta);

    @Query(value = "SELECT * FROM ACESSO WHERE porta_id =?1 and usuario_id =?2",nativeQuery = true)
    public Acesso findByUsuarioPorta(int idPorta, int idUsuario);

}
