package com.ex2.acesso.services;

import com.ex2.acesso.models.Acesso;
import com.ex2.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    public Acesso cadastrarAcesso(Acesso pAcesso)
    {
        return acessoRepository.save(pAcesso);
    }

    public void deletarAcesso(Acesso pAcesso)
    {
        acessoRepository.delete(pAcesso);
    }

    public Iterable<Acesso> consultarAcessoPorPorta(int idPorta)
    {
        return acessoRepository.findByPorta_id(idPorta);
    }

    public Iterable<Acesso> consultarAcessoPorUsuario(int idUsuario)
    {
        return acessoRepository.findByUsuario_id(idUsuario);
    }

    public Acesso consultarAcessoPorPortaeUsuario(int idPorta, int idUsuario)
    {
        return acessoRepository.findByUsuarioPorta(idPorta, idUsuario);
    }


}
